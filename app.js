var express = require('express');
var app = express();

app.use(express.static('public'));
var cookieParser = require('cookie-parser');
var session = require('express-session');
var logger = require('morgan');
var passport = require('passport');
var mongoose = require('mongoose');
var cors = require('cors');
var bodyParser = require('body-parser');

var MongoStore = require('connect-mongo')(session);

app.use(cors());
mongoose.connect('mongodb://127.0.0.1/movment', function (err) {
    if(err){
        throw err;
    } else {
        console.log('connect to the database');
    }
});


app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(session({
    store: new MongoStore({url : 'mongodb://localhost/movment'}),
    secret: 'ultranova',
    resave : true,
    saveUninitialized: true}
));

app.use(passport.initialize());
app.use(passport.session());

// view engine setup
app.set('view engine', 'ejs');

require('./Authentification/Passport')(passport);
require('./routes/Routes')(app, passport);

app.listen(8000);

// deleting unused avatar
var deleteScript = require('./scripts/Delete');
deleteScript.DeleteUnusedAvatar();

// testing push notification
var notificationScript = require('./scripts/Notification');
// var post_data = {
//     'user' : 'mikeycrostar',
//     'type': 'android',
//     'token': 'co6dxexoqHc:APA91bGvqaqXwk2JH3IKODI9jZqbWLbtOWhcfprsMRtjxjAaM89TymKIh81BhMkidRRLY7uUp6ATfxENomqS9AQi-9TfgPIXnBi_YFDaTDPCu2HveQkZaJBUXA2cwLRrqVXX75ejes4E'
// };
// var post_data = {
//     'user' : 'biji',
//     'type': 'android',
//     'token': 'f-PpB92wgBk:APA91bEcqMXFnga6O9dHvapcXebt6pXVVMkPfKE_BrS4fuYIsNcfbr3eSWCoSTQVT_6OUS298aDEANGXYU_6fv2WiLzsovq2pHmobTdM5SpM5KaA8urGazgsLWoywKMKe0_eiaJWvmLsJ'
// };
// var post_data = {
//     'user' : 'mikeycrostar3',
//     'type': 'ios',
//     'token': 'f37e07eae4ba2b6b6d2b8db21dc1b7c639092700293b74ffc747b0e1d2ddc3c0'
// };
// notificationScript.PostRequest(post_data, '/subscribe');
var post_data2 = {
    'users' : [
        'biji', 'mikeycrostar3'
    ],
    'android': {
        'collapseKey': 'optional',
        'data': {
            'message': 'wesh wesh yo'
        }
    },
    'ios': {
        'badge': 0,
        'alert': 'wesh wesh yozzzzzzzzzzzzz',
        'sound': 'soundName'
    }
};
// notificationScript.PostRequest(post_data2, '/send');
