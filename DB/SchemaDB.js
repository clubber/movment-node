var mongoose = require('mongoose');
var bcrypt  = require('bcrypt');
var Schema = mongoose.Schema;

var UserSchema = mongoose.Schema({
    local:{
        email: { type: String },
        password: { type : String},
        firstName: String,
        lastName: String,
        resetPasswordToken: String,
        resetPasswordExpires: Date,
        birthDate: Date,
        gender: String,
        preferences: [],
        adress1: String,
        adress2: String,
        city: String,
        country: String,
        registrationDate: {type: Date, default:Date.now },
        isAdmin:{ type: Boolean, default: false },
        isOrganizer: { type: Boolean, default: false },
        isModerator: { type: Boolean, default: false },
        banner: { type: String, default: null },
        place:[],
        pict: [],
        musicChoices: [],
        eventChoices: [],
        followees:[]
    },
    organiser: {
        bio: { type: String, default: null },
        company: { type: String, default: '' },
        typeParty: [],
        genreMusic: []
    },
    bankAccount:{

    },
    facebook: {
        id: {type: String, unique: true},
        token: String,
        email: String,
        gender: String,
        lastName: String,
        firstName: String,
        photo: {},
        friends: {}
    },
    right:{
        events:[{ event: {type: Schema.Types.ObjectId, ref:'Event'}}],
        access:[{ event:{type: Schema.Types.ObjectId, ref:'Event'}}]
    },
    activities: {
        events:[ {value: String, event:{type: Schema.Types.ObjectId, ref: 'Event'}}]
    },
    likes : {
        snaps : [{ type: Schema.Types.ObjectId, ref: 'Snap' }]
},
    notifications : [ { notif: { type: Schema.Types.ObjectId, ref: 'Notification' } , read: { type: String, default: "false" } } ]
});

var SnapSchema = mongoose.Schema({
    name: String,
    creator: { type: Schema.Types.ObjectId, ref: 'User', default: null},
    dateCreation: {type: Date, default: Date.now},
    description: String,
    details: { type : Schema.Types.ObjectId, ref:'Event', name: String},
    comments: [ { type: Schema.Types.ObjectId, ref:'Comment' } ],
    place: {},
    like: [ { type: Schema.Types.ObjectId, ref: 'User' } ]
});

var ErrorSchema = mongoose.Schema({
    date: { type : Date, default: Date.now },
    type: String,
    errorUrl : String,
    errorMessage : String,
    stackTrace: String,
    version: String,
    cause: String
});

var guestListSchema = mongoose.Schema({
    dateCreation: { type: Date, default: Date.now },
    type: String,
    user: { },
    event: { }
});

var CommentSchema = mongoose.Schema({
    type: String,
    comment: String,
    date: { type: Date, default: Date.now },
    creator: { }
});

var NotificationSchema = mongoose.Schema({
    author: {type: Schema.Types.ObjectId, ref: 'User', default: null },
    payload: {},
    message: String,
    type: String,
    creationDate: {type: Date, default: Date.now }
});

UserSchema.methods.addEvent = function(event, value){
    event.participants.push({value: value, user: this._id });
    this.activities.events.push({value: value, event: event._id});

    this.save(function (err) {
        if(err)
            throw err;
        event.save(function (err) {
            if (err)
                throw err;
        })
    })
};

UserSchema.methods.rmEvent = function(event){
    var userEvent = this.activities.events;

    for (var i = 0; i < userEvent.length; i++){
        if (event.id == userEvent[i].event){
            this.activities.events.splice(i, 1);
        }
    }

    var userId = '' + this._id;
    for (var y = 0; y < event.participants.length; y++){
       var eventUserId = '' + event.participants[y].user;
        if (userId === eventUserId){
            event.participants.splice(y, 1);
        }
    }

    this.save(function (err) {
        if (err)
            throw err;
        event.save(function (err) {
            if (err)
                throw err;
        });
    });

};

UserSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(9));
};

UserSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

var EventSchema = mongoose.Schema({
    name: String,
    dateStart: Date,
    dateEnd: Date,
    description: String,
    position: String,
    place:[],
    namePlace: String,
    type:[],
    photos: [],
    organiser: { type: String, default: null },
    phoneNumber:[],
    notificationId: Number,
    address: String,
    peopleNumber: Number,
    price_m: Number,
    price_f: Number,
    teaser: { type: String, default: null },
    grantedBy: {type: Schema.Types.ObjectId, ref: 'User', default: null},
    owner: {type: Schema.Types.ObjectId, ref: 'User', default: null},
    isAdmin: {type: Schema.Types.ObjectId, ref: 'User', default: null},
    participants: [{ value: { type: String, default: null }, user:{type: Schema.Types.ObjectId, ref:  'User'}}],
    active: {type: Boolean, default: false },
    status: { type: String, default: 'Brouillon'}
});


EventSchema.methods.granted = function (admin) {
    this.grantedBy = admin._id;
    admin.right.access.push(this._id);

    this.save(function (err) {
        if (err)
            throw err;
        admin.save(function (err) {
            if (err)
                throw err;
        })
    })
};

EventSchema.methods.organizer = function (owner) {
    this.owner = owner._id;
    owner.right.events.push(this._id);
        this.save(function (err) {
            if (err)
                throw err;
            owner.save(function (err) {
                if (err)
                    throw err;
            });
        });
};

/*
EventSchema.methods.isAdmin = function (admin) {
    this.owner.events.push(admin._id);
    admin.right.events.push(this._id);

    this.save(function (err) {
        if (err)
            throw err;
        admin.save(function (err) {
            if (err)
                throw err;
        });
    });
};
*/

var pushAssociationSchema = new mongoose.Schema({
    user: {
        type: 'String',
        required: true
    },
    type: {
        type: 'String',
        required: true,
        enum: ['ios', 'android'],
        lowercase: true
    },
    token: {
        type: 'String',
        required: true
    }
});

module.exports = {
    Error : mongoose.model('Error', ErrorSchema),
    User : mongoose.model('User', UserSchema),
    Event: mongoose.model('Event', EventSchema),
    Snap: mongoose.model('Snap', SnapSchema),
    Notification: mongoose.model('Notification', NotificationSchema),
    Comment: mongoose.model('Comment', CommentSchema),
    PushNotif: mongoose.model('PushAssociation', pushAssociationSchema),
    GuestList: mongoose.model('GuestList', guestListSchema)
};
