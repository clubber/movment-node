var express = require('express');
var app = express();
app.set('view engine', 'ejs');
var querystring = require('querystring');
var http = require('http');

module.exports = {
    PostRequest: function(post_data, cmd) {
      // An object of options to indicate where to post to
      var post_options = {
          'host': '127.0.0.1',
          'port': '8001',
          'path': cmd,
          'method': 'POST',
          'headers': {
              'Content-Type': 'application/json',
              'Content-Length': JSON.stringify(post_data).length
            }
      };

      // Set up the request
      var post_req = http.request(post_options, function(res) {
          res.setEncoding('utf8');
          res.on('data', function (chunk) {
              console.log('Response: ' + chunk);
          });
      });

      // post the data
      post_req.write(JSON.stringify(post_data));
      post_req.end();
    },
    
    GetRequest : function (data, cmd, cb) {
        var post_options = {
            'host': '127.0.0.1',
            'port': '8001',
            'path': cmd,
            'method': 'GET',
            'headers': {
                'Content-Type': 'application/json',
                'Content-Length': JSON.stringify(data).length
            }
        };

        // Set up the request
        var post_req = http.get(post_options, function(res) {
            var str = '';
            res.setEncoding('utf8');
            res.on('data', function (chunk) {
                str += chunk;
            });

            res.on('end', function () {
                cb(str);
            })
        });

        // post the data
       // post_req.write(JSON.stringify(data));
        post_req.end();
    }
};
