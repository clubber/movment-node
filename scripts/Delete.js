// Delete unused avatar from the database

var Db = require('../DB/SchemaDB');
var _ = require('underscore');
var fs = require('fs');

// function to get the diff of 2 array
function arr_diff (a1, a2) {
    var a = [], diff = [];
    for (var i = 0; i < a1.length; i++) {
        a[a1[i]] = true;
    }
    for (var i = 0; i < a2.length; i++) {
        if (a[a2[i]]) {
            delete a[a2[i]];
        } else {
            a[a2[i]] = true;
        }
    }
    for (var k in a) {
        diff.push(k);
    }
    return diff;
};

var avatarFolder = "public/avatar/";

module.exports = {
    DeleteUnusedAvatar: function() {
      Db.User.find({}, function (err, users) {
          if (err)
              throw err;

          // creating an array with all existing files in avatar
          var allAvatar = [];
          fs.readdirSync(avatarFolder).forEach(function(file) {
            allAvatar.push(file);
          });

          // creating an array with all avatar linked with user
          var userAvatar = [];
          _.each(users, function(user) {
            var avatar = user.local.pict[0];
            if (avatar != "undefined" && avatar != null)
              userAvatar.push(avatar);
          })

          // creating an array of unused avatar
          var unusedAvatar = arr_diff(allAvatar, userAvatar);

          // deleting unused avatar
          _.each(unusedAvatar, function(avatar) {
            fs.unlink(avatarFolder + avatar, function (err) {});
          })
      });
    }
};
