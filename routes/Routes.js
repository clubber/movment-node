/**
 * Created by mikeycrostar on 09/01/2016.
 */
var Db = require('../DB/SchemaDB');
var push = require('../scripts/Notification');
var jwt = require('jwt-simple');
var moment = require('moment');
var ffmpeg = require('fluent-ffmpeg');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var fs = require('fs');
var User = Db.User;
var Events = Db.Event;
var Snaps = Db.Snap;
var Notification = Db.Notification;
var Comment = Db.Comment;
var GuestList = Db.GuestList;
var Error = Db.Error;
var Push = Db.PushNotif;
var async = require('async');
var crypto = require('crypto');
var nodemailer = require('nodemailer');
var FB = require('fb');
var request = require('request');
var AuthToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI1NjRmNGI0Mi1jOWM4LTQyY2EtYWZmMS01MjNhY2Q0YjFlZjAifQ.uGXKVWcUt-KSYZJ3t-hN1HkwtPaXJ7WsD_s_vKOvmZ8';
var _ = require('underscore');


module.exports = function (app, passport) {

    app.all('*', function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.header('Access-Control-Allow-Credentials', true);
        next();
    });

    app.get('/user/activities', function (req, res) {
        var token = getToken(req.headers);
        if (token) {
            var decoded = jwt.decode(token, 'thesecretis');
            User.findById(decoded._id, 'activities', function (err, user) {
                if (err)
                    throw err;
                res.status(200).send({success: true, activities: user.activities});
            })
        } else {
            return res.status(403).send({success: false, msg: 'No token provided.'});
        }
    });

    app.get('/users/', function (req, res) {
        var token = getToken(req.headers);
         if (token) {
             //deleteUsers();
         pushIonic();
            User.find({}, function (err, user) {
                if (err)
                    throw err;
                return res.status(200).send({success: true, users: user});
            });
         } else {
             return res.status(403).send({success: false, msg: 'No token provided.'});
         }
    });


    app.post('/error', function (req, res) {
        var newError = new Error();
        newError.type =  !_.isUndefined(req.body.type) ? req.body.type : 'App';
        newError.errorUrl =  req.body.errorUrl;
        newError.errorMessage = req.body.errorMessage;
        newError.stackTrace = req.body.stackTrace;
        newError.version = req.body.version;
        newError.cause = req.body.cause;

        newError.save(function (err) {
            if (err)
                throw err;
            res.status(200).send({success: true, msg: 'error save'});
        });
    });

    app.get('/error', function (req, res) {
        var token = getToken(req.headers);
        if (token){
            Error.find({}).sort('-date').limit(5).exec(function (err, errors) {
                if (err)
                    throw err;
                res.status(200).send({ success : true, error : errors });

            });
        } else {
            return res.status(403).send({success: false, msg: 'No token provided.'});
        }
    });

    app.post('/user/update', function (req, res) {
        var token = getToken(req.headers);
        if (token) {
            var decoded = jwt.decode(token, 'thesecretis');
            var user = req.body.user;
            delete user['_id'];
            User.findByIdAndUpdate(decoded._id, { $set: req.body.user }, function (err, user) {
                if (err)
                    throw err;
                res.status(200).send({success: true, msg: user});
            })
        } else {
            return res.status(403).send({success: false, msg: 'No token provided.'});
        }
    });

    app.post('/user/update/:id', function (req, res) {
        var token = getToken(req.headers);
        if (token) {
            var user = req.body.user;
            delete user['_id'];
            User.findByIdAndUpdate(req.params.id, { $set: req.body.user }, function (err, user) {
                if (err)
                    throw err;
                res.status(200).send({success: true, msg: user});
            })
        } else {
            return res.status(403).send({success: false, msg: 'No token provided.'});
        }
    });

    app.post('/upload', multipartMiddleware, function (req, res) {
        var n = req.files;
        fs.rename(n.file.path, '/var/www/movment/public/eventImage/' + n.file.originalFilename, function (err) {
            if (err) throw err;
            fs.stat('/var/www/movment/public/eventImage/' + n.file.originalFilename, function (err, stats) {
                if (err) throw err;
                res.json({success: true, message: "The file has been uploaded"});
            });
        });
    });

    app.get('/organiser/event', function (req, res) {
        var token = getToken(req.headers);
        if (token){
            var decoded = jwt.decode(token, 'thesecretis');
            Events.find({ owner : decoded._id }, function (err, events) {
                return res.status(200).send({ success: true, events: events });
            });
        } else {
            return res.status(403).send({ success: false, msg: 'No token provided.' });
        }
    });

    app.get('/fbevent/:eventId', function (req, res) {
        var token = getToken(req.headers);
        if (token){
            var decoded = jwt.decode(token, 'thesecretis');
            User.findById(decoded._id,'facebook', function (err, user) {
                FB.api('/' + req.params.eventId,{ fields: 'cover,name,description,end_time,start_time,category,place,owner', access_token:user.facebook.token }, function (response) {
                    if (response && !response.error){
                        return res.status(200).send({ success: true, msg: response });
                    } else {
                        return res.status(403).send({ success: false, msg: 'error' });
                    }
                });
            });
        } else {
            return res.status(403).send({ success: false, msg: 'No token provided.' });
        }
    });

    app.post('/event/', function (req, res) {
        var token = getToken(req.headers);

        if (token) {
            var decoded = jwt.decode(token, 'thesecretis');

            Events.count({}, function (err, count) {
            var newEvent = new Events();
            newEvent.name = req.body.event.name;
            newEvent.dateStart = req.body.event.dateStart;
            newEvent.dateEnd = req.body.event.dateEnd;
            newEvent.description = req.body.event.description;
            newEvent.address = req.body.event.address;
            newEvent.price_m = req.body.event.price_m;
            newEvent.price_f = req.body.event.price_f;
            newEvent.place = req.body.event.place;
            newEvent.organiser = req.body.event.organiser;
            newEvent.phoneNumber.push(req.body.event.phone1);
            newEvent.namePlace = req.body.event.namePlace;
            newEvent.position = req.body.event.position;
            newEvent.type = req.body.event.type;
            newEvent.peopleNumber = req.body.event.peopleNumber;
            newEvent.photos.unshift(!_.isUndefined(req.body.event.file) && !_.isUndefined(req.body.event.file.$ngfName)
                ? req.body.event.file.$ngfName : '');
            newEvent.teaser = !_.isUndefined(req.body.event.video) && !_.isUndefined(req.body.event.video.$ngfName)
                ? req.body.event.video.$ngfName : '';
            newEvent.notificationId =  count + 1;
            newEvent.status = 'Waiting';
            getUser(decoded, function (user) {
                newEvent.organizer(user);
            });
                newEvent.save(function (err) {
                    if (err)
                        throw err;
                    res.sendStatus(200);
                });
            });
        } else {
            return res.status(403).send({success: false, msg: 'No token provided.'});
        }
    });


    app.post('/update/event', function (req, res) {
        var token = getToken(req.headers);
        if (token) {
            var decoded = jwt.decode(token, 'thesecretis');
            var event = _.clone(req.body.event);
            delete event['_id'];
            Events.findByIdAndUpdate(req.body.event._id, { $set: event }, function (err, event) {
                if (err)
                    throw err;
               return res.status(200).send({success: true, msg: event});
            })
        } else {
            return res.status(403).send({success: false, msg: 'No token provided.'});
        }
    });

    app.post('/update/event/:status', function (req, res) {
        var token = getToken(req.headers);
        if (token) {
            var decoded = jwt.decode(token, 'thesecretis');
            var event = _.clone(req.body.event);
            delete event['_id'];
            event.status = req.params.status;
            Events.findByIdAndUpdate(req.body.event._id, { $set: event }, function (err, event) {
                if (err)
                    throw err;
               return res.status(200).send({success: true, msg: event});
            })
        } else {
            return res.status(403).send({success: false, msg: 'No token provided.'});
        }
    });

    app.delete('/event/:id', function (req, res) {
        Events.remove({ _id: req.params.id }, function (err) {
            if (err)
                throw err;
            return res.status(200).send({success: true, msg: 'Évènement supprimé'});
        });
    });

    app.put('/event/', function (req, res) {
        var token = getToken(req.headers);

        if (token) {
            var decoded = jwt.decode(token, 'thesecretis');

        } else {
            return res.status(403).send({success: false, msg: 'No token provided.'});
        }
    });

    app.get('/event/:eventId', function (req, res, next) {
        var token = getToken(req.headers);
        if (token) {
            Events.findOne({_id: req.params.eventId}).populate({ path: 'owner', select: 'local facebook organiser'}).exec(function (err, event) {
                res.status(200).send({success: true, events: event});
            });
        } else {
            return res.status(403).send({success: false, msg: 'No token provided.'});
        }
    });

    app.get('/events/', function (req, res, next) {
        var token = getToken(req.headers);
        if (token) {
            Events.find({}, function (err, events) {
                if (err)
                    throw err;
                res.status(200).send({success: true, events: events})
            });
        } else {
            return res.status(403).send({success: false, msg: 'No token provided.'});
        }
    });

    app.get('/events/:status', function (req, res, next) {
        var token = getToken(req.headers);
        if (token) {
            Events.find({ status: req.params.status }, function (err, events) {
                if (err)
                    throw err;
                res.status(200).send({success: true, events: events})
            });
        } else {
            return res.status(403).send({success: false, msg: 'No token provided.'});
        }
    });

    app.post('/event/interest/', function (req, res) {
        var token = getToken(req.headers);
        if (token) {
            var decoded = jwt.decode(token, 'thesecretis');
            User.findById(decoded._id, function (err, user) {
                if (err)
                    throw err;
                Events.findById(req.body.eventId, function (err, event) {
                    if (err)
                        throw err;
                    user.addEvent(event, req.body.value);
                    messageToFbFriends(user.facebook.token, "Your friend " + user.facebook.firstName + ' ' + user.facebook.lastName + ' is going to ' + event.name, {type: 'Event', id: event._id });
                    res.status(200).send({success: true, msg: "On y va !"});
                });
            })
        } else {
            return res.status(403).send({success: false, msg: 'No token provided.'});
        }
    });


    app.post('/push/sub', function (req, res) {
        request('http://127.0.0.1:8001/users/' + req.body.userId +'/associations', function (error, response, body) {

            var body = JSON.parse(body);
            var result = _.find(body.associations, function (device) {
               return device.token == req.body.token;
            });
            if (_.isUndefined(result)){
            push.PostRequest({
                    user: req.body.userId,
                    type: req.body.type,
                    token: req.body.token
                }, '/subscribe');
             return res.status(200).send({ success: true, msg: 'subscribe'});
            } else {
            return res.status(304).send({ success: true, msg: 'subscribe yet'});
            }
        });
    });

    app.post('/push/ted', function (req, res) {
        Notif = new Notification();

        Notif.author = null;
        Notif.payload = req.body.payload;
        Notif.message = req.body.message;
        Notif.type = req.body.type;

       Notif.save(function (err) {
            if (err)
                throw err;
            updateAllUserNotification(Notif);
            push.PostRequest({
                'Users': [],
                'android': {
                    'collapseKey': 'optional',
                    'data': {
                        'message': req.body.message,
                        'custom': req.body.payload ? _.extend(req.body.payload, { type : req.body.type }) : {}
                    }
                },
                'ios': {
                    'badge': 1,
                    'alert': req.body.message,
                    'sound': 'soundName',
                    'payload': req.body.payload ? _.extend(req.body.payload, { type : req.body.type }) : {}
                }
            }, '/send');
           res.status(200).send({success: true});
        });

    });

    app.get('/push/notification', function(req, res){
        var token = getToken(req.headers);
        if (token){
            var decoded = jwt.decode(token, 'thesecretis');
            User.findOne({_id: decoded._id }, 'notifications').populate('notifications.notif').then(function(user){
                res.status(200).send({success: true, notifs: user.notifications });
            });
        } else {
            res.status(500).send({success: false, msg: 'No token provided'});
        }
    });

    app.post('/event/desinterest/', function (req, res) {
        var token = getToken(req.headers);
        if (token) {
            var decoded = jwt.decode(token, 'thesecretis');
            User.findById(decoded._id, function (err, user) {
                if (err)
                    throw err;
                Events.findById(req.body.eventId, function (err, event) {
                    if (err)
                        throw err;
                    user.rmEvent(event);
                    res.status(200).send({success: true, msg: "On n'y va plus !"});
                });
            })
        } else {
            return res.status(403).send({success: false, msg: 'No token provided.'});
        }
    });

    app.post('/event/type/', function (req, res) {
        var eventsType = [];
        var token = getToken(req.headers);
        if (token) {
            Events.find({ status: 'Active'}).populate({ path: 'owner', select: 'local facebook organiser'}).exec(function (err, events) {
                if (err)
                    throw err;

                for (var i = 0; i < events.length; i++) {
                    var event = events[i];
                    for (var y = 0; y < event.type.length; y++) {
                        var type = event.type[y];
                        if (type == req.body.type && moment(event.dateStart).isSameOrAfter(moment()) ) {
                            eventsType.push(event);
                        }
                    }
                }
                res.status(200).send({success: true, events: eventsType});
            });
        } else {
            return res.status(403).send({success: false, msg: 'No token provided.'});
        }
    });

    app.post('/forgot', function (req, res, next) {
        async.waterfall([
            function (done) {
                crypto.randomBytes(20, function (err, buf) {
                    var token = buf.toString('hex');
                    done(err, token);
                });
            },
            function (token, done) {
                User.findOne({ 'local.email': req.body.email }, function (err, user) {
                    if (!user) {
                        return res.status(403).send({success: false, msg: 'No account with that email address exists.'});
                    }

                    user.local.resetPasswordToken = token;
                    user.local.resetPasswordExpires = Date.now() + 3600000; // 1 hour

                    user.save(function (err) {
                        done(err, token, user);
                    });
                });
            },
            function (token, user, done) {
                var smtpTransport = nodemailer.createTransport({
                    host: 'smtp.gmail.com',
                    port: 465,
                    secure: true, // use SSL
                    auth: {
                        user: 'mathias@movment.io',
                        pass: 'dAadRA/dei'
                    }
                });
                var mailOptions = {
                    to: user.local.email,
                    from: 'nepasrepondre@movment.io',
                    subject: 'Node.js Password Reset',
                    text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                    'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
                    'http://organisateur.movment.io/#/forget/' + token + '\n\n' +
                    'If you did not request this, please ignore this email and your password will remain unchanged.\n'
                };
                smtpTransport.sendMail(mailOptions, function (err) {
                    done(err, 'done');
                    res.status(200).send({success: true, msg: 'An e-mail has been sent to ' + user.local.email + ' with further instructions.'});
                });
            }
        ], function (err) {
            if (err) return next(err);
        });
    });

    app.get('/tomorrow/', function (req, res, next) { // tomorrow event
        var token = getToken(req.headers);
        var result = [];
        if (token) {
            Events.find({}).populate({ path: 'owner', select: 'local facebook organiser'}).exec(function (err, events) {
                if (err)
                    throw err;

                for (var i = 0; i < events.length; i++) {
                    if (moment().add(1, 'd').isSame(events[i].dateStart, 'day')) {
                        result.push(events[i]);
                    }
                }
                return res.status(200).send({success: true, events: result});
            });
        } else {
            return res.status(403).send({success: false, msg: 'No token provided.'});
        }
    });

    app.post('/user/musicChoices/', function (req, res) {
        var token = getToken(req.headers);
        if (token){
            User.find({ 'local.musicChoices': { $in: req.body.musics } }, 'local facebook', function (err, Users) {
                if (err)
                    throw err;
                return res.status(200).send({ success: true, users: Users});
            });
        } else {
            return res.status(403).send({success: false, msg: 'No token provided.'});
        }

    });


    app.post('/reset/:token', function(req, res) {
        async.waterfall([
            function(done) {
                User.findOne({ 'local.resetPasswordToken': req.params.token, 'local.resetPasswordExpires': { $gt: Date.now() } }, function(err, user) {
                    if (!user) {
                        return res.status(403).send({success: false, msg: 'Password reset token is invalid or has expired.'});
                    }

                    user.local.password = user.generateHash(req.body.password);
                    user.local.resetPasswordToken = undefined;
                    user.local.resetPasswordExpires = undefined;

                    user.save(function(err) {
                            done(err, user);
                    });
                });
            },
            function(user, done) {
                var smtpTransport = nodemailer.createTransport({
                    host: 'smtp.gmail.com',
                    port: 465,
                    secure: true, // use SSL
                    auth: {
                        user: 'mathias@movment.io',
                        pass: 'dAadRA/dei'
                    }
                });
                var mailOptions = {
                    to: user.local.email,
                    from: 'nepasrepondre@movment.io',
                    subject: 'Votre mot de passe a été changé !',
                    text: 'Hello,\n\n' +
                    'This is a confirmation that the password for your account ' + user.local.email + ' has just been changed.\n'
                };
                smtpTransport.sendMail(mailOptions, function(err) {
                    done(err);
                });
            }
        ], function(err) {
            res.status(200).send({success: true, msg: 'Success! Your password has been changed.'});
        });
    });

    app.get('/tonight/', function (req, res, next) { // tonight event
        var token = getToken(req.headers);
        var result = [];
        if (token) {
            Events.find({}).populate({ path: 'owner', select: 'local facebook organiser'}).exec(function (err, events) {
                if (err)
                    throw err;
                _.each(events, function (event, index) {
                     if(moment().isBetween(event.dateStart, event.dateEnd) || moment().isSame(event.dateStart, 'day')){
                        result.push(event);
                     }
                    if (index + 1 == events.length){
                        res.status(200).send({success: true, events: result});
                    }
                });
            });
        } else {
            return res.status(403).send({success: false, msg: 'No token provided.'});
        }
    });

    app.get('/wk/', function (req, res, next) { // Ce weekend
        var result = [];
        var friday = moment().day(5);
        var sunday = moment().day(7);

        var token = getToken(req.headers);
        if (token) {
            Events.find({}).populate({ path: 'owner', select: 'local facebook organiser'}).exec(function (err, events) {
                if (err)
                    throw err;

                _.each(events, function (event, index) {

                    if (moment(event.dateStart).isBetween(friday, sunday,'day', '[]')) {
                        result.push(event);
                    }
                    if (events.length - 1 == index){
                        res.status(200).send({success: true, events: result})
                    }
                });

            });
        } else {
            return res.status(403).send({success: false, msg: 'No token provided.'});
        }
    });

    app.get('/auth/facebook', passport.authenticate('facebook-app', {scope: ['email', 'user_friends']}));


    app.post('/auth/fb', function (req, res) {

      FB.api("me", { fields: 'id,gender,email,first_name,last_name,friends,picture', access_token: req.body.token }, function (response) {
          if (response && !response.error) {
              User.findOne({ "facebook.id": response.id }, function (err, user) {
                if (err)
                    throw err;
                if (user == null) {
                  var newUser = new User();
                  newUser.facebook.id = response.id;
                  newUser.facebook.token = req.body.token;
                  newUser.facebook.email = response.email;
                  newUser.facebook.gender = response.gender;
                  newUser.facebook.lastName = response.last_name;
                  newUser.facebook.firstName = response.first_name;
                  newUser.facebook.photo = response.picture;
                  newUser.facebook.friends = response.friends;
                  newUser.save(function (err) {
                  if (err) {
                      return res.json({success: false, msg: 'Username already exists.'});
                  }
                      messageToFbFriends(req.body.token, 'Your friend ' + newUser.facebook.firstName + ' ' + newUser.facebook.lastName + ' joined Movment', {type:'friends'});
                      return res.json({success: true, msg: 'Successful created new user.', user: newUser, token: 'JWT ' + jwt.encode(newUser, 'thesecretis') });
                  })
                } else {
                    if (!_.isUndefined(user.facebook)){
                        user.facebook.token = req.body.token;
                        user.save(function (err) {
                            if (err)
                                throw err;
                            return res.status(200).send({success: true, user: user, token: 'JWT ' + jwt.encode({ _id: user._id,
                                facebook: {
                                token : user.facebook.token
                                }
                            }, 'thesecretis') });
                        });
                    } else {
                        return res.status(200).send({success: true, user: user, token: 'JWT ' + jwt.encode({
                            _id: user._id,
                            facebook: {
                                token: user.facebook.token
                            }
                        }, 'thesecretis') });
                    }
                }
            });
          } else {
            return res.json({success: false, msg: response.error});
          }
        }
      );
    });
    

    var connections = [];
    app.get('/auth/facebook/callback/', passport.authenticate('facebook-app'), function (req, res) {
        connections.push({user: req.user, code: req.query.code});
        res.sendStatus(200);
    });

    app.post('/auth/facebook/verify_login', function (req, res) {
        for (var i = 0; i < connections.length; i++) {
            if (req.body.code == connections[i].code) {
                var token = jwt.encode(connections[i].user, 'thesecretis');
                return res.status(200).json({success: true, token: 'JWT ' + token});
            }
        }
    });

    app.post('/login', function (req, res, next) {
        User.findOne({'local.email': req.body.email}, function (err, user) {
            if (err) {
                throw err;
            }

            if (req.body.password == '' && req.body.email == '') {
                return res.status(302).send({success: false, message: "Empty field"});
            }

            if (user === null) {
                return res.status(302).send({success: false, message: "No user found"});
            }
            if (!user.validPassword(req.body.password)) {
                return res.status(302).send({success: false, message: "Invalid Password"});
            } else {
                var token = jwt.encode({ _id: user._id,
                    facebook: {
                        token : user.facebook.token
                    }
                }, 'thesecretis');
                return res.status(200).json({success: true, token: 'JWT ' + token, user: user});
            }
        })
    });

    app.post('/signup', function (req, res, next) {
        User.findOne({'local.email': req.body.email}, function (err, user) {
            if (!req.body.email || !req.body.password) {
                return res.json({success: false, msg: 'Please pass name and password.'});
            }
            if (user) {
                return res.json({success: false, msg: 'Username already exists.'});

            } else {
                var newUser = new User();
                newUser.local.email = req.body.email;
                newUser.local.password = newUser.generateHash(req.body.password);
                newUser.local.firstName =!_.isUndefined(req.body.user) && !_.isUndefined(req.body.user.firstName) ? req.body.user.firstName : '';
                newUser.local.lastName = !_.isUndefined(req.body.user) && !_.isUndefined(req.body.user.lastName) ? req.body.user.lastName : '';
                newUser.local.birthDate = !_.isUndefined(req.body.user) && !_.isUndefined(req.body.user.birthDate) ? req.body.user.birthDate : moment().toDate() ;
                newUser.local.gender = !_.isUndefined(req.body.user) && !_.isUndefined(req.body.user.gender) ? req.body.user.gender : '';
                newUser.organiser.company = !_.isUndefined(req.body.user) && !_.isUndefined(req.body.user.company) ? req.body.user.company : '';
                newUser.local.registrationDate = moment().toDate();
                newUser.save(function (err) {
                    if (err) {
                        return res.json({success: false, msg: 'Username already exists.'});
                    }
                    res.json({success: true, msg: 'Successful created new user.'});
                })
            }
        })
    });

    app.get('/user/:id', function (req, res) {
        var token = getToken(req.headers);
        if (token) {
            User.findOne({_id: req.params.id}, function (err, user) {
                if (err)
                    throw err;
                return res.status(200).send({success: true, msg: user});
            });
        } else {
            return res.status(403).send({success: false, msg: 'No token provided.'});
        }
    });

    app.delete('/user/:id', function (req, res) {
        User.remove({ _id: req.params.id }, function (err) {
            if (err)
                throw err;
            return res.status(200).send({success: true, msg: 'Utilisateur supprimé'});
        });
    });

    app.get('/fb/update', function(req, res){
        var token = getToken(req.headers);
        var decoded = jwt.decode(token, 'thesecretis');
        FB.api('oauth/access_token', {
            client_id: '484274888364234',
            client_secret: 'd9e4ba46397d1e2561b3206e794dee60',
            grant_type: 'fb_exchange_token',
            fb_exchange_token: decoded.facebook.token
        }, function (fbres) {
            if(!fbres || fbres.error) {
                return res.status(304).send({success: false, msg: fbres.error});
            }
            User.findOne({_id: decoded._id }, function (err, user) {
                if (err){
                    throw err;
                }
                user.facebook.token = fbres.access_token;
                user.save(function (err) {
                    if (err)
                        throw err;
                   return res.status(200).send({success: true, msg: 'New fb token provided'});
                });

            });
        });
    });

    app.get('/profile', passport.authenticate('jwt', {session: false}), function (req, res, next) {
        var token = getToken(req.headers);
        if (token) {
            var decoded = jwt.decode(token, 'thesecretis');
            User.findOne({_id: decoded._id}, function (err, user) {
                if (err)
                    throw err;
                if (!user) {
                    return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
                } else {
                    res.json({success: true, user: user});
                }
            });
        } else {
            return res.status(403).send({success: false, msg: 'No token provided.'});
        }
    });

    app.post('/upload/avatar/', multipartMiddleware, function (req, res) {
        var n = req.files;

        fs.rename(n.file.path, '/var/www/movment/public/avatar/' + n.file.originalFilename, function (err) {
            if (err) throw err;
            fs.stat('/var/www/movment/public/avatar/' + n.file.originalFilename, function (err, stats) {
                if (err) throw err;
                res.json({success: true, message: "The file has been uploaded"});
            });
        });
    });

    app.post('/upload/snap/', multipartMiddleware, function (req, res) {
        var n = req.files;
        var token = getToken(req.headers);
        if (token){
            var decoded = jwt.decode(token, 'thesecretis');
            var Snap = new Snaps();
            Snap.name = n.file.originalFilename;
            Snap.description = req.body.description;
            Snap.place = {
                eventName : req.body.name,
                eventId : req.body.eventId
            };
            Snap.creator = decoded._id;

            var command = ffmpeg(n.file.path).size('640x?').autoPad();

            command.videoCodec('libx264').audioCodec('libfaac').on('error', function(err) {
                console.log('An error occurred: ' + err.message);
            })
                .on('end', function() {
                    Snap.save(function (err) {
                        if(err) throw err;
                        if (decoded.facebook.token){
                            messageToFbFriends(decoded.facebook.token, 'Your friend ' + decoded.facebook.firstName + ' ' + decoded.facebook.lastName + ' has published an owl moment', {type : 'owl', id: Snap._id} );
                        }
                        return res.status(200).json({success: true, snaps: 'ok'});
                    });
                })
                .save('/var/www/movment/public/video/' + n.file.originalFilename);

        }
    });

    app.post('/upload/teaser/', multipartMiddleware, function (req, res) {
        var n = req.files;
        var token = getToken(req.headers);
        if (token){
            var command = ffmpeg(n.file.path).size('640x?').autoPad();
            command.videoCodec('libx264').audioCodec('libfaac').on('error', function(err) {
                console.log('An error occurred: ' + err.message);
            })
            .on('end', function() {
                return res.status(200).json({success: true, video: 'ok'});
            })
            .save('/var/www/movment/public/video/' + n.file.originalFilename);

        }
    });

    app.post('/snap/like/:snapId', function (req, res) {
        var token = getToken(req.headers);
        var decoded = jwt.decode(token, 'thesecretis');
        Snaps.findOne({_id : req.params.snapId }, function (err, snaps) {
            if (err)
                throw err;
            User.findOne({_id: decoded._id}, function (err, user) {
                if (err)
                    throw err;
                if (req.body.queryingFor == 'like'){
                    user.likes.snaps.push(req.params.snapId);
                    snaps.like.push(decoded._id);
                } else {
                    for (var i = 0; i < user.likes.snaps.length; i++){
                        if (req.params.snapId == user.likes.snaps[i]._id){
                            user.likes.snaps.splice(i, i);
                        }
                    }
                    for (var y = 0; y < snaps.like.length; y++){
                        if (decoded._id == snaps.like[y]._id){
                            snaps.like.splice(y, 1);
                        }
                    }
                }

                snaps.save(function (err) {
                    if (err)
                        throw err;
                    user.save(function (err) {
                        if (err)
                            throw err;
                        return res.status(200).json({success: true, msg: 'Done'});
                    })
                })
            });

        });
    });

    app.get('/organisers', function (req, res) {
        var token = getToken(req.headers);
        if (token){
            User.find({ 'local.isOrganizer' : true }, function (err, organisers) {
                if (err)
                    throw err;
                return res.status(200).json({ success : true, msg: organisers});
            });
        } else {
            return res.status(304).json({success: false, msg: 'No token provided'});
        }
    });

    app.get('/snaps', function (req, res) {
        var token = getToken(req.headers);
        if (token){
           Snaps.find({}).populate('creator comments').then(function (snaps) {
               return res.status(200).json({success: true, snaps: snaps});
           });
        } else {
            return res.status(304).json({success: false, msg: 'No token provided'});
        }
    });


    app.post('/people/follow/:id', function (req, res) {
        var token = getToken(req.headers);
        if (token){
            var decoded = jwt.decode(token, 'thesecretis');
            User.findOne({_id: decoded._id}, function (err, user) {
                if (err)
                    throw err;
                user.local.followees.push(req.params.id);
                user.save(function (err, user) {
                    return res.status(200).json({success: true, user: user});
                });
            });
        } else {
            return res.status(304).json({success: false, msg: 'No token provided'});
        }
    });

    app.post('/people/unfollow/:id', function (req, res) {
        var token = getToken(req.headers);
        if (token){
            var decoded = jwt.decode(token, 'thesecretis');
            User.findOne({_id: decoded._id}, function (err, user) {
                if (err)
                    throw err;
                for (var i = 0; i < user.local.followees.length; i++){
                    if (req.params.id == user.local.followees[i]){
                        user.local.followees.splice(i, 1);
                        user.save(function (err, user) {
                            return res.status(200).json({success: true, user: user});
                        });
                    }
                }
            });
        } else {
            return res.status(304).json({success: false, msg: 'No token provided'});
        }
    });

    app.get('/people/follower', function (req, res) {

        var token = getToken(req.headers);
        if (token){
            var decoded = jwt.decode(token, 'thesecretis');
            myid = [];
            myid.push(decoded._id);
            User.find({ 'local.followees': { $in: myid } }, function (err, user) {
                if (err)
                    throw err;
                return res.status(200).json({success: true, user: user});
            });
        } else {
            return res.status(304).json({success: false, msg: 'No token provided'});
        }

    });

    app.get('/people/followees', function (req, res) {
        var token = getToken(req.headers);
        if (token){
            var decoded = jwt.decode(token, 'thesecretis');
            User.findOne({ _id: decoded._id },'local.followees', function (err, user) {
                if (err)
                    throw err;
                return res.status(200).json({success: true, user: user.local.followees });
            });
        } else {
            return res.status(304).json({success: false, msg: 'No token provided'});
        }
    });

    app.get('/snap/:id', function (req, res) {
        var token = getToken(req.headers);
        if (token){
           Snaps.findOne({ _id: req.params.id }).populate('creator comments').then(function (snap) {
               if (err)
                   throw err;
               return res.status(200).json({success: true, snaps: snap});
           });
        } else {
            return res.status(304).json({success: false, msg: 'No token provided'});
        }
    });


    app.get('/guestList/', function (req, res) {
        var token = getToken(req.headers);
        if (token){
        GuestList.find({ }, function (err, guestLists) {
            if (err)
                throw err;
            return res.status(200).json({success: true, guestList: guestLists});
        });
        } else {
        return res.status(304).json({success: false, msg: 'No token provided'});
        }
    });

    app.post('/guestList/', function (req, res) {
        var token = getToken(req.headers);
        if (token){
            var guestlist = new GuestList();

            guestlist.type = req.body.type;
            guestlist.event = req.body.event;
            guestlist.user = req.body.user;

            guestlist.save(function (err) {
                if (err) {
                    return res.status(200).json({success: false, msg: 'Username already exists.'});
                }
                res.status(304).json({success: true, msg: 'Successful created new user.'});
            });

        } else {
            return res.status(304).json({success: false, msg: 'No token provided'});
        }
    });

    app.get('/guestList/:userId', function (req, res) {
        var token = getToken(req.headers);
        if (token){
            GuestList.find({ 'user._id': req.params.userId }, function (err, guestlist) {
                if (err) {
                    return res.status(200).json({success: false, msg: 'Username already exists.'});
                }
                res.status(304).json({success: true, guestlist: guestlist });
            });
        } else {
            return res.status(304).json({success: false, msg: 'No token provided'});
        }
    });


    app.post('/comment/snap/:id', function (req, res) {
        var token = getToken(req.headers);
        if (token){
           Snaps.findOne({_id: req.params.id }, function (err, snap) {
               if (err)
                   throw err;
                var newComment = new Comment();
               newComment.type = 'snap';
               newComment.comment = req.body.comment;
               newComment.creator = req.body.creator;
               newComment.save(function (err) {
                   if (err)
                       throw err;
                   interactionWithYourActivities(snap.creator, 'Someone has commented your video!', {type :'snapComment', id: req.params.id});
                   snap.comments.push(newComment._id);
                   snap.save(function (err) {
                       if (err)
                           throw err;
                       return res.status(200).json({success: true, snaps: snap})
                   })
               });
           });
        } else {
            return res.status(304).json({success: false, msg: 'No token provided'});
        }
    });

    app.post('/user/invite/', function (req, res) {
        User.findById(req.body.userId, function (err, user) {
            Notif = new Notification();
            Notif.author = null;
            Notif.payload = req.body.payload;
            Notif.message = req.body.message;
            Notif.type = req.body.type;

            Notif.save(function (err) {
                if (err)
                    throw err;
                push.PostRequest({
                    'Users': [ user._id ],
                    'android': {
                        'collapseKey': 'optional',
                        'data': {
                            'message': req.body.message,
                            'custom': req.body.payload ? _.extend(req.body.payload, { type : req.body.type }) : {}
                        }
                    },
                    'ios': {
                        'badge': 1,
                        'alert': req.body.message,
                        'sound': 'soundName',
                        'payload': req.body.payload ? _.extend(req.body.payload, { type : req.body.type }) : {}
                    }
                }, '/send');
                res.status(200).send({success: true});
            });
        })
    });

    app.post('/notif/read/:id', function (req, res) {
        var token = getToken(req.headers);
        var decoded = jwt.decode(token, 'thesecretis');
        User.findOne({ _id: decoded._id }, function (err, user) {
            var notifs = user.notifications;
             _.each(notifs, function (notif, index) {
                if (notif.notif == req.params.id) {
                    notif.read = 'true';
                    user.notifications[index] = notif;
                    user.save(function (err) {
                        if (err)
                            throw err;
                        res.status(200).json({success: true, notification: user.notifications });
                    });
                }
            });
        });
    });

    var getToken = function (headers) {
        if (headers && headers.authorization) {
            var parted = headers.authorization.split(' ');
            if (parted.length === 2) {
                return parted[1];
            } else {
                return null;
            }
        } else {
            return null;
        }
    };

    var getUser = function (user, cb) {
        User.findById(user._id, function (err, user) {
            if (err)
                throw err;
            cb(user);
        });
    };

    var getFbfriends = function (token, cb) {
        FB.api("me", { fields: 'friends', access_token: token }, function (response) {
                if (response && !response.error) {
                    var friendsList = response.friends.data;
                    var friendsId = [];
                    User.find({}, '_id facebook.id', function (err, fbUser) {
                        _.each(fbUser, function (user) {
                            _.each(friendsList, function (fb) {
                                if (fb.id == user.facebook.id) {
                                    friendsId.push("" + user._id);
                                }
                            });
                        });
                        cb({ success: true, listFriend:friendsId});
                    });
                } else {
                    cb({ success: false, listFriend:[]});
                }
            }
        );
    };

    var messageToFbFriends = function (token, message, custom) {
        getFbfriends(token, function (call) {
            if (call.success){
                Notif = new Notification();
                Notif.author = null;
                Notif.payload = custom;
                Notif.message = message;
                Notif.type = custom.type;

                var options = {
                    uri: 'http://127.0.0.1:8001/send',
                    method: 'POST',
                    body: {
                        "users": call.listFriend,
                        "android": {
                            "collapseKey": "optional",
                            "data": {
                                "message": message,
                                "custom":  custom
                            }
                        },
                        "ios": {
                            "badge": 0,
                            "alert": message,
                            "sound": "soundName",
                            "payload":custom
                        }
                    },
                    json: true
                };

                Notif.save(function (err) {
                    if (err) throw err;
                    updateUserNotification(Notif, call.listFriend);
                });
                request(options, function (err,body) {
                    return 1;
                })
            } else {
                return 0;
            }
        });
    };


    var interactionWithYourActivities = function (user, message, custom) {
                var usr = [];
                usr[0]= user;

                Notif = new Notification();
                Notif.author = null;
                Notif.payload = custom;
                Notif.message = message;
                Notif.type = custom.type;

                var options = {
                    uri: 'http://127.0.0.1:8001/send',
                    method: 'POST',
                    body: {
                        "users": usr,
                        "android": {
                            "collapseKey": "optional",
                            "data": {
                                "message": message,
                                "custom":  custom
                            }
                        },
                        "ios": {
                            "badge": 1,
                            "alert": message,
                            "sound": "soundName",
                            "payload":custom
                        }
                    },
                    json: true
                };

                Notif.save(function (err) {
                    if (err) throw err;
                    updateUserNotification(Notif, usr);
                });
                request(options, function (err,body) {
                    return 1;
                });

    };


    function updateUserNotification(notification, list) {
        _.each(list, function (id) {
            User.findOne({'_id': id }, function (err, user) {
                user.notifications.push({ notif: notification._id });
                user.save(function (err) {
                   if (err) throw err;
                })
            });
        });
    }

    function updateAllUserNotification(notification){
        User.find({}, function (err, users) {
            _.each(users, function (user) {
                user.notifications.push({ notif: notification._id });
                user.save(function (err) {
                    if (err) throw err;
                });
            })
        })
    }

   //removenotif();

    function removenotif(){
        User.find({}, function (err, users) {
            _.each(users, function (user) {
                user.notifications = [];
                user.save(function (err) {
                    if (err) throw err;
                });
            });
        })
    }

    function getNotifs (user, cb) {
        var notifs = [];
        _.each(user.notifications, function (notif, index) {
            Notification.findOne({ _id: notif._id }, function (err, dataNotif) {
                notifs.push({ notif: dataNotif,  read: notif.read});
                if (user.notifications.length == index + 1){
                    cb(notifs);
                }
            });
        });
    }


    function changeStatus() {
        Events.find({}, function (err, events) {
            _.each(events, function (event) {
                if (moment().isAfter(event.dateEnd)) {
                    event.status = 'Outdated';
                } else {
                    event.status = 'Active';
                }

                event.save(function (err) {
                    if (err){
                        throw err;
                    }
                })
            })
        })
    }

    function pushIonic() {
        var app_id = '9a297abd';
       // deleteUsers();

        Push.find({}, function (err, pushs) {
            _.each(pushs, function (push) {
                User.findById(push.user, 'local facebook', function (err, user) {
                    var options1 = {
                        method: 'POST',
                        url: 'https://api.ionic.io/users',
                        json: {
                            app_id: app_id,
                            email: user.local.email && _.isUndefined(user.local) ? user.local.email : user.facebook.email == 'noadress@mail.fr' ? user._id + '@' + 'mail.com' : user.facebook.email,
                            password: user._id
                        },
                        headers: {
                            'Authorization': 'Bearer ' + AuthToken,
                            'Content-Type': 'application/json'
                        }
                    };

                    request(options1, function(err, response, body) {

                    });


                });
            });
            });
    }


    function deleteUsers() {
        var propertiesObject = {
            page_size: 1000,
            page: 1
        };

        var options1 = {
            method: 'GET',
            url: 'https://api.ionic.io/users',
            qs:propertiesObject,
            headers: {
                'Authorization': 'Bearer ' + AuthToken,
                'Content-Type': 'application/json'
            }
        };

        request(options1, function (err, response, body) {
            _.each(JSON.parse(body).data, function (user) {
                var options2 = {
                    method: 'DELETE',
                    url: 'https://api.ionic.io/users/' + user.uuid,
                    headers: {
                        'Authorization': 'Bearer ' + AuthToken,
                        'Content-Type': 'application/json'
                    }
                };
                request(options2, function (err, response, body) {
                    if (err) throw err;
                    console.log('done');
                });
            });


        });



    }

    function statsUserLikeKind() {

            function getData(cb){
                var data = [];
                    User.find({}, 'local', function (err, users) {
                    _.each(users, function (user, index) {
                        _.each(user.local.eventChoices, function (event) {
                            data.push(event);
                        });

                        _.each(user.local.musicChoices, function (music) {
                            data.push(music);
                        });
                        if (users.length == index + 1){
                            cb(data);
                        }
                    });
                });
            }

            getData(function (data) {
                var count = {};
                data.forEach(function (i) {
                    count[i] = (count[i]||0)+1;
                });
                console.log(count);
            });
    }
};
