/**
 * Created by mikeycrostar on 09/01/2016.
 */

var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var _ = require('underscore');
var db = require('../DB/SchemaDB');

var User = db.User;

module.exports = function (passport) {
    passport.serializeUser(function(user, done){
        done(null, user.id);
    });

    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            done(err, user);
        })
    });

  /* passport.use('app-signup', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    },
        function(req, email, password, done){
            console.log("user");
            process.nextTick(function () {
            User.findOne({'local.email': email}, function (err, user) {
                console.log("user");
                if (err){
                    return done(err);
                }
                if (user){
                    return done(null, false, "User already exist");
                } else {
                    var newUser = new User();
                    newUser.local.email = email;
                    newUser.local.password = newUser.generateHash(password);
                    newUser.save(function (err) {
                     if (err){
                         throw err;
                     }
                        return done(null, newUser);
                    })
                }
            })
            })
        }
    ));

    passport.use('app-login',new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    },
        function (req, email, password, done) {
        process.nextTick(function () {
            User.findOne({ 'local.email': email}, function (err, user) {
                if (err){
                    return done(err);
                }
                if (!user){
                    return done(null, false, {message : "No user found"});
                }
                if (!user.validPassword(password)){
                    console.log('ok');
                    return done(null, false, {message : "Invalid Password"});
                }
                return done(null, user);
            })
        })
    }
    ));*/

    var opts = {};
    opts.jwtFromRequest = ExtractJwt.versionOneCompatibility({ token : "MY_CUSTOM_BODY_FIELD" });
    opts.secretOrKey = 'thesecretis';
    passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
        User.findOne({id: jwt_payload.id}, function(err, user) {
            if (err) {
                return done(err, false);
            }
            if (user) {
                done(null, user);
            } else {
                done(null, false);
            }
        });
    }));

    passport.use('facebook-app', new FacebookStrategy({
        clientID: '484274888364234',
        clientSecret:'d9e4ba46397d1e2561b3206e794dee60',
        callbackURL: 'http://movment.io:8000/auth/facebook/callback',
        profileFields: ['id', 'displayName','name', 'gender', 'friends', 'photos', 'emails']
    },
        function (accessToken, refreshToken, profile, done) {
        process.nextTick(function () {
            User.findOne({'facebook.id': profile.id}, function (err, user) {
                if (err){
                    return done(err);
                }
                if (user){
                    return done(null, user);
                }
                else {
                    var newUser = new User();
                    newUser.facebook.id = profile.id;
                    newUser.facebook.token = accessToken;
                    newUser.facebook.firstName = profile.name.givenName;
                    newUser.facebook.lastName = profile.name.familyName;
                    newUser.facebook.gender = profile.gender;
                    newUser.facebook.email = profile.emails && profile.emails[0].value != 'undefined' ? profile.emails[0].value : 'noadress@mail.fr' ;
                    newUser.facebook.photo = profile.photos;
                    newUser.facebook.friends = profile._json.friends;
                    newUser.save(function (err) {
                        if (err){
                            throw err;
                        }
                        return done(null, newUser);
                    })
                }
            });
        })
    }))
};